# GNUmakefile for Guile-BAUX

# Copyright (C) 2010, 2011, 2017, 2020 Thien-Thi Nguyen
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

GUILE := guile

ifeq "guile" "$(GUILE)"
setguile :=
else
setguile := GUILE='$(GUILE)' #
endif

all: guile-baux-tool docs

check install installcheck:
	@echo 'Nothing to do for "make $@"'.

docs:
	$(MAKE) -C doc info

gbaux-do	:= $(setguile)$(realpath guile-baux/gbaux-do)
program 	:= -perm /+x
support 	:= -name '*.scm'
tmp-dep-files	:= $(addsuffix -deps, program support)

%-deps:
	find guile-baux -type f $($*) -print0 \
	  | $(gbaux-do) inner-upstream -z --common '(guile-baux)' \
	      --leaf --format "(define $*-deps '~S)~%" > $@ \
	|| { rm -f $@ ; exit 1 ; }

guile-baux-tool: guile-baux-tool.in ChangeLog
	$(MAKE) $(tmp-dep-files)
	git log --pretty=format:'%ci %h' \
	  | sed 's/:.. [+-].... /./;s/[-:]//g;y/ /./' \
	  | sed 's/.*/s|VERSION|&|/' \
	  > TMP
	sed -f TMP -e '4s|@dir@|'"$$(pwd)"'|' \
		   -e '3s|@GUILE@|$(GUILE)|' \
		   -e '/;;; program deps/r program-deps' \
		   -e '/;;; support deps/r support-deps' \
	  $< > $@
	chmod +x $@
	rm TMP $(tmp-dep-files)

symlink: guile-baux-tool
	@test "$(to)" || { echo 'usage: $(MAKE) symlink to=FILENAME' ; false ; }
	ln -s $(abspath $<) $(to)

update:
	git pull
	$(MAKE)

# See manual: (info "(guile-baux) Personal Use").
f0-dispatch = gbaux-do
f0-bin = $(HOME)/bin
f0-programs =

mux-like-wrapper: guile-baux/gbaux-do
	{ echo '#!/bin/sh' ;				\
	  echo 'program=$$(basename $$0)' ;		\
	  echo 'exec $(abspath $<) $$program "$$@"' ;	\
	} > $(f0-bin)/$(f0-dispatch)
	chmod +x $(f0-bin)/$(f0-dispatch)
	if [ -n "$(f0-programs)" ] ; then	\
	  cd $(f0-bin) ;			\
	  for p in $(f0-programs) ;		\
	    do ln -sf $(f0-dispatch) $$p ;	\
	  done ;				\
	fi

extradist := $(addprefix doc/, guile-baux.info version.texi ref.texi snippets)

dist: guile-baux-tool docs
	dd=guile-baux-$$(./$< --version | sed 's/.* //;1q') ;	\
	rm -rf $$dd ; mkdir $$dd ;				\
	cp -p --parents $(extradist) $$(git ls-files) $$dd ;	\
	tar cf $$dd.tar.lz --auto $$dd ;			\
	rm -rf $$dd ; tar tf $$dd.tar.lz

# GNUmakefile ends here

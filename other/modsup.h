/* modsup.h --- provide ‘GH_DEFPROC’ and ‘GH_MODULE_LINK_FUNC’
   serial 2

   Copyright (C) 2012, 2017 Thien-Thi Nguyen

   This is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef HAVE_GUILE_MODSUP_H

#include <guile/modsup.h>

#define MOD_INIT_LINK_THUNK  GH_MODULE_LINK_FUNC

#else  /* !defined HAVE_GUILE_MODSUP_H */

#define GH_DEFPROC(fname, primname, req, opt, var, arglist, docstring) \
  SCM_SNARF_HERE (static SCM fname arglist;)                           \
  SCM_DEFINE (fname, primname, req, opt, var, arglist, docstring)

#define GH_MODULE_LINK_FUNC(module_name, fname_frag, module_init_func)  \
void                                                                    \
scm_init_ ## fname_frag ## _module (void);                              \
void                                                                    \
scm_init_ ## fname_frag ## _module (void)                               \
{                                                                       \
  /* Make sure strings(1) finds module name at bol.  */                 \
  static const char modname[] = "\n" module_name;                       \
  scm_register_module_xxx (1 + modname, module_init_func);              \
}

#define MOD_INIT_LINK_THUNK(pretty, frag, func)         \
void scm_init_ ## frag ## _module (void);               \
void scm_init_ ## frag ## _module (void) { func (); }

#endif  /* !defined HAVE_GUILE_MODSUP_H */

/* modsup.h ends here */

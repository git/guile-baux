;;; bv.scm

;; Copyright (C) 2012, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux bv)
  #:export (make-bv
            bv-set!
            bv-ref))

(define make-bv
  (cond-expand (guile-2 make-bitvector)
               (else (if (defined? 'make-bitvector)
                         make-bitvector
                         (lambda (sz init)
                           (make-uniform-vector sz #t init))))))

(define bv-set!
  (cond-expand (guile-2 bitvector-set!)
               (else (if (defined? 'bitvector-set!)
                         bitvector-set!
                         uniform-vector-set!))))

(define bv-ref
  (cond-expand (guile-2 bitvector-ref)
               (else (if (defined? 'bitvector-ref)
                         bitvector-ref
                         uniform-vector-ref))))

;;; bv.scm ends here

#!/bin/sh
exec ${GUILE-guile} -s $0 "$@" # -*- scheme -*-
!#
;;; mko --- create nodes for "other" bits

;; Copyright (C) 2013, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: ./mko [-o OUTFILE] INFILE

;;; Code:

(use-modules
 ((ice-9 rdelim) #:select (read-line))
 ((ice-9 regex) #:select (match:end
                          match:suffix))
 ((srfi srfi-1) #:select (find-tail
                          find
                          car+cdr))
 ((srfi srfi-11) #:select (let-values))
 ((srfi srfi-13) #:select (string-take
                           string-concatenate
                           string-tokenize
                           string-join
                           string-suffix?))
 ((srfi srfi-14) #:select (char-set-complement
                           string->char-set)))

(define CL (command-line))
(define CL-COUNT (length CL))

(define (lose!)
  (error "bad or missing args -- see source!"))

(or (memq CL-COUNT '(2 4))
    (lose!))

(cond ((= 4 CL-COUNT)
       (or (string=? "-o" (cadr CL))
           (lose!))
       (set-current-output-port (open-output-file (caddr CL)))
       (set-cdr! CL (cdddr CL))))

(define (fso s . args)
  (apply simple-format #t s args))

(define (symbol<- string)
  ;; Do it this way to guarantee results of same STRING to be ‘eq?’.
  (read (open-input-string string)))

(define (another-line-thunk . args)
  (lambda ()
    (apply read-line args)))

(define tok: (make-object-property))

(define (scan-source port)              ; => (RECORD ...)

  ;; Source (text) has the form:
  ;;
  ;;   ##: NAME[ARGLIST]
  ;;   ##  [TOK ...]
  ;;   ##
  ;;   ## BODY
  ;;   ## [...]
  ;;   ##
  ;;   AC_DEFUN(...)
  ;;
  ;; NAME is a symbol.  ARGLIST is optional and has the form:
  ;;
  ;;   (ARG0[,ARG1...])
  ;;
  ;; i.e., paren, one or more comma-separated symbols, paren.
  ;; Note that there is no provision for "optional arg" syntax.
  ;; The line w/ TOK ... is optional.  Each TOK is an identifier.
  ;; There must be a single "blank" line prior to the BODY lines.
  ;; BODY is one or more text lines (each ending w/ ‘#\newline’).
  ;;
  ;; RECORD is a Scheme pair.  The car has the form:
  ;;
  ;;   (NAME [ARG0 ...])
  ;;
  ;; The cdr a string, made from the BODY lines (sans bol "##").

  ;; NB: .m4-specific scanning
  (define trigger (make-regexp "^##: "))
  (define inside (make-regexp "^## ?"))

  (define another-line (another-line-thunk port 'concat))

  (define decruft
    (let ((cs (char-set-complement (string->char-set "()\n ,"))))
      ;; decruft
      (lambda (s)
        (delete "" (string-tokenize s cs)))))

  (define (tok! object)
    (let ((ls (string-tokenize (match:suffix
                                (regexp-exec inside
                                             (another-line))))))
      (and (pair? ls)
           ;; discard empty line
           (another-line))
      (set! (tok: object) ls)
      (list object)))

  (let loop ((recs (list))
             (cur #f))
    (let ((line (another-line)))
      (cond
       ;; done?
       ((eof-object? line)
        (reverse! recs))                ; rv
       ;; region in progress?
       (cur
        (cond
         ;; more?
         ((regexp-exec inside line)
          => (lambda (m)
               (loop recs
                     (cons (substring line (match:end m))
                           cur))))
         ;; no more
         (else
          (loop (let ((cur (reverse! (find-tail (lambda (s)
                                                  ;; 1 => "\n" (blank line)
                                                  (< 1 (string-length s)))
                                                cur))))
                  (acons (car cur)
                         (string-concatenate (cdr cur))
                         recs))
                #f))))
       ;; enter region?
       ((regexp-exec trigger line)
        => (lambda (m)
             (let ((ls (decruft (substring line (match:end m)))))
               (loop recs (tok! (cons (symbol<- (car ls))
                                      (cdr ls)))))))
       ;; keep looking
       (else
        (loop recs #f))))))

(define (process/texg recs another-line)

  (define (find-record string)
    (let ((name (symbol<- string)))
      (find (lambda (rec)
              (eq? name (caar rec)))
            recs)))

  (define (display-record rec)
    (for-each (lambda (var)
                (fso "@vindex ~A~%" var))
              (tok: (car rec)))
    (let-values (((head body) (car+cdr rec)))
      ;; NB: .m4-specific rendering
      (fso "@deffn {m4 Macro} ~A~A~%~A@end deffn~%"
           (car head)
           (string-join (cdr head) " " 'prefix)
           body)))

  (define gyre (make-regexp "^ "))

  (let loop ()
    (let ((line (another-line)))
      (cond
       ;; done?
       ((eof-object? line))
       ;; replace
       ((regexp-exec gyre line)
        => (lambda (m)                  ; in the wabe
             (display-record (find-record (substring line (match:end m))))
             (loop)))
       ;; as-is
       (else
        (display line)
        (loop))))))

(define (process/list src another-line)
  (fso "@node       ~A~%" src)
  (fso "@subsection ~A~%" src)
  (fso "These ~A are provided:~%~%"
       (if (string-suffix? ".m4" src)
           "Autoconf macros"
           "C macros and @code{#define}s"))
  (fso "@example~%")
  (let loop ()
    (let ((line (another-line)))
      (or (eof-object? line)
          (let ((extra (string-index line #\space)))
            (or (string-null? line)
                (char=? #\; (string-ref line 0))
                (fso "@vindex ~A~%~A~%"
                     (if extra
                         (string-take line extra)
                         line)
                     line))
            (loop)))))
  (fso "@end example~%")
  (newline))

(define (process port)
  (let* ((filename (port-filename port))
         (dir (dirname filename))
         (dot (string-index-right filename #\.))
         (ext (substring filename dot))
         (src (basename filename ext)))
    (case (symbol<- (substring ext 1))
      ((texg)
       ;; At the moment, ‘texg’ scanning and rendering is .m4-specific.
       ;; Make sure to fail loudly if we ever try it on .h files.
       ;; TODO: Either (a) refactor to handle .h properly; or (b) write
       ;; ‘m4-tsar’ and ‘h-tsar’, revert to simple (.list only) ‘mko’,
       ;; convert .texg to .texh, and use ‘tsin’ to generate .texi files.
       (or (string-suffix? ".m4" src)
           (error (simple-format
                   #f "Sorry, ‘texg’ for ~A not (yet?) implemented"
                   src)))
       (process/texg (call-with-input-file
                         (in-vicinity (in-vicinity dir "../../other/")
                                      src)
                       scan-source)
                     (another-line-thunk port 'concat)))
      ((list) (process/list src (another-line-thunk port))))))

(call-with-input-file (cadr CL) process)
(exit #t)

;;; mko ends here
